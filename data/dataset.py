import os
#import cv2
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms.functional as TF
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import TensorDataset, DataLoader, Dataset
from PIL import Image
import pdb
from numpy import random
import glob
from skimage.transform import resize
import numpy as np
from skimage.exposure import equalize_hist as equalize

def pad(data):
	
	pImg = torch.zeros((1,640,512))
	h = (int((640-data.shape[1])/2) )
	w = int((512-data.shape[2])/2)  
	if w == 0: 
		pImg[0,np.abs(h):(h+data.shape[1]),:] = (data[0]) 
	else: 
		pImg[0,:,np.abs(w):(w+data.shape[2])] = (data[0])
	return pImg


class covidData(Dataset):
	def __init__(self,data_dir = '/home/jwg356/erda/COVID-19/workDir/',
					split='train',process=False,
					hflip=False,vflip=False,rot=0,p=0.5,
					transform=None):

		super().__init__()
#		pdb.set_trace()
		self.data_dir = data_dir
		self.mask_dir = data_dir+'masks/'+split+'/'
		self.hflip = hflip
		self.vflip = vflip
		self.rot = rot
		self.split = split
		self.mask = sorted(glob.glob(self.mask_dir+'*.png'))
		self.data = [f.split('/')[-1].replace('_mask.png','.png') for f in self.mask]
		self.w = 512
		self.h = 640
		self.p = p
		if process:
			self.process()
		self.images, self.targets =\
						torch.load(self.data_dir+'processed/'+self.split+'.pt')			

	def __len__(self):
		return len(self.mask)

	def __getitem__(self, index):
		image, label = self.images[index], self.targets[index]
		image, label = TF.to_pil_image(image), TF.to_pil_image(label)

		do_hflip = random.random() > self.p
		do_vflip = random.random() > self.p
		do_rot   = random.random()
		
		if self.hflip and do_hflip:
			image,label = TF.hflip(image), TF.hflip(label)

		if self.vflip and do_vflip:
			image,label = TF.vflip(image), TF.vflip(label)

		if (self.rot > 0) and (do_rot > self.p):
			image,label = TF.rotate(image,self.rot*do_rot),\
							TF.rotate(label,self.rot*do_rot)
		image, label = TF.to_tensor(image), TF.to_tensor(label)
		image = TF.normalize(image,mean=[0.5],std=[0.5])
		return image, label

		
	def process(self):
			N = len(self.mask)
			images = torch.zeros((N,1,self.h,self.w))
			labels = torch.zeros((N,1,self.h,self.w))
			for index in range(N):
				image = Image.open(self.data_dir+'equalized/'+self.data[index])
				label = Image.open(self.mask[index])
				h = int(image.height/(image.width/self.w))
				if h > self.h:
					self.w = int(image.width/(image.height/self.h))
				image, label = TF.resize(image,(self.h,self.w)), \
								TF.resize(label,(self.h,self.w))
				image, label = TF.to_tensor(image), TF.to_tensor(label)

				image, label = pad(image), pad(label)
				images[index],labels[index] = image, label

			torch.save((images,labels),self.data_dir+'processed/'+self.split+'.pt')

class lungData(Dataset):
	def __init__(self,data_dir = '/home/jwg356/covid/data/', process=False,
					hflip=False,vflip=False,rot=0,p=0.5,rMask=0,
					transform=None):

		super().__init__()
		self.h = 640
		self.w = 512
		self.data_dir = data_dir
		self.hflip = hflip
		self.vflip = vflip
		self.rot = rot
		self.rMask = rMask
		if process:
			self.process()
		self.data, self.targets = torch.load(data_dir+'processed/lungData.pt')
		self.p = p

	def __len__(self):
		return len(self.targets)

	def __getitem__(self, index):

		image, label = self.data[index], self.targets[index]
		image, label = TF.to_pil_image(image), TF.to_pil_image(label)

		do_hflip = random.random() > self.p
		do_vflip = random.random() > self.p
		do_rot   = random.random()
		do_rMask = random.random()
		
		if self.hflip and do_hflip:
			image,label = TF.hflip(image), TF.hflip(label)

		if self.vflip and do_vflip:
			image,label = TF.vflip(image), TF.vflip(label)

		if (self.rot > 0) and (do_rot > self.p):
			image,label = TF.rotate(image,self.rot*do_rot),\
							TF.rotate(label,self.rot*do_rot)

		image, label = TF.to_tensor(image), TF.to_tensor(label)

		if (self.rMask > 0) and (do_rMask > self.p):
			hIdx = int(do_rMask*self.h/2)
			wIdx = int(do_rMask*self.w/2)
			image[0,hIdx-self.rMask:hIdx+self.rMask,wIdx-self.rMask:wIdx+self.rMask] = 0
		return image, label

	def process(self):
			mask = sorted(glob.glob(self.data_dir+'raw/masks/*.png'))
			data = [f.split('/')[-1].replace('_mask.png','.png') for f in mask]
			N = len(mask)
			images = torch.zeros((N,1,self.h,self.w))
			labels = torch.zeros((N,1,self.h,self.w))
			for index in range(N):
				image = Image.open(self.data_dir+'raw/equalized/'+data[index])
				label = Image.open(mask[index])
				h = int(image.height/(image.width/self.w))
				if h > self.h:
					self.w = int(image.width/(image.height/self.h))
				image, label = TF.resize(image,(self.h,self.w)), \
								TF.resize(label,(self.h,self.w))
				image, label = TF.to_tensor(image), TF.to_tensor(label)

				image, label = pad(image), pad(label)
				images[index],labels[index] = image, label

			torch.save((images,labels),self.data_dir+'processed/lungData.pt')

class Drive(Dataset):
	def __init__(self, split='train', data_dir = '/datadrive/raghav/retinaDataset/', 
			transform=None, target_transform=None):
		super().__init__()

		self.data_dir = data_dir
		self.transform = transform
		self.target_transform = target_transform
		self.data, self.targets = torch.load(data_dir+'retina_'+split+'_new.pt')
		
	def __len__(self):
		return len(self.targets)

	def __getitem__(self, index):

		image, label_mask = self.data[index], self.targets[index]
		if self.transform is not None:
			image = self.transform(image)
			label_mask = self.target_transform(label_mask)
		return image, label_mask[:2], label_mask[[2]]





