import torch

import numpy as np
import torch.nn as nn
from models.unet import Unet
import time
import glob
import pdb
import argparse
torch.manual_seed(42)
np.random.seed(42)
from pydicom import dcmread
from skimage.transform import resize
import matplotlib.pyplot as plt
import torchvision.transforms.functional as TF
from skimage.exposure import equalize_hist as equalize

def loadDCM(f):
	wLoc = 512
	dcm = dcmread(f).pixel_array
	dcm = dcm/dcm.max()
	dcm = equalize(dcm)

	hLoc = int((dcm.shape[0]/(dcm.shape[1]/wLoc)))
	if hLoc > 640:
		hLoc = 640
		wLoc = int((dcm.shape[1]/(dcm.shape[0]/hLoc)))

	img = resize(dcm,(hLoc,wLoc))
	img = torch.Tensor(img)
	pImg = torch.zeros((640,512))
	h = (int((640-hLoc)/2))
	w = int((512-wLoc)/2)
	if w == 0:
		pImg[np.abs(h):(h+img.shape[0]),:] = img
	else:
		pImg[:,np.abs(w):(w+img.shape[1])] = img	

	imH = dcm.shape[0]
	imW = dcm.shape[1]
#	imgT = TF.normalize(imgT.unsqueeze(0),mean=[0.5],std=[0.5])
	pImg = pImg.unsqueeze(0).unsqueeze(0)
	return pImg, h, w, hLoc, wLoc, imH, imW

def saveMask(f,img,h,w,hLoc,wLoc,imH,imgW):
	img = img.data.numpy()

	if w == 0:
		img = resize(img[np.abs(h):(h+hLoc),:],(imH,imW))
	else:
		img = resize(img[:,np.abs(w):(w+wLoc)],(imH,imW))

	plt.gray()
	plt.imsave(f,img>0.5)

parser = argparse.ArgumentParser()
parser.add_argument('--data', type=str, default='.', help='Path to DCM files')
parser.add_argument('--model', type=str, default='.', help='Path to trained model')
parser.add_argument('--saveLoc', type=str, default='.', help='Path to save predictions')
parser.add_argument('--batch_size', type=int, default=96, help='Batch size')

args = parser.parse_args()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

net = Unet(input_channels=1, num_classes=1, 
				num_filters=[16,32,64,128], 
				apply_last_layer=True, 
				padding=True, norm=True, 
				initializers={'w':'he_normal', 'b':'normal'})
net.load_state_dict(torch.load(args.model,map_location=device))
net.to(device)

#pdb.set_trace()
files = sorted(glob.glob(args.data+'*.DCM'))

for f in files[:100]:
		img, h, w, hLoc, wLoc, imH, imW = loadDCM(f)
		img = img.to(device)
		mask = torch.sigmoid(net(img,False))
		f = args.saveLoc+f.split('/')[-1].replace('.DCM','_mask.png')
		saveMask(f,mask.squeeze(),h,w,hLoc,wLoc,imH,imW)

