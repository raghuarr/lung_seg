import torch

import numpy as np
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from data.dataset import lungData
import torch.nn as nn
from models.unet import Unet
from utils.utils import l2_regularisation,ged
import time
from utils.tools import makeLogFile, writeLog, dice_loss,dice,binary_accuracy
import pdb
import matplotlib.pyplot as plt
import argparse
torch.manual_seed(42)
np.random.seed(42)


parser = argparse.ArgumentParser()
parser.add_argument('--epochs', type=int, default=200, help='Number of training epochs')
parser.add_argument('--batch_size', type=int, default=96, help='Batch size')
parser.add_argument('--lr', type=float, default=1e-4, help='Learning rate')
parser.add_argument('--data_path', type=str, default='data/',help='Path to data.')
parser.add_argument('--aug',action='store_true', default=False,help='Use data aug.')

args = parser.parse_args()

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
if args.aug:
	dataset = lungData(data_dir=args.data_path,
					hflip=True,vflip=True,rot=180,p=0.1,rMask=100)
	print("Using data augmentation....")
else:
	dataset = lungData(data_dir=args.data_path)
	print("No augmentation...")
dataset_size = len(dataset)
indices = list(range(dataset_size))
split = int(np.floor(0.25 * dataset_size))

np.random.shuffle(indices)
valid_indices, train_indices = \
				indices[:split], indices[split:]

train_sampler = SubsetRandomSampler(train_indices)
valid_sampler = SubsetRandomSampler(valid_indices)

train_loader = DataLoader(dataset, batch_size=args.batch_size, sampler=train_sampler)
valid_loader = DataLoader(dataset, batch_size=args.batch_size, sampler=valid_sampler)

print("Number of train/valid patches:", 
				(len(train_indices),len(valid_indices)))

fName = time.strftime("%Y%m%d_%H_%M")

net = Unet(input_channels=1, num_classes=1, 
				num_filters=[16,32,64,128], 
				apply_last_layer=True, 
				padding=True, norm=True, 
				initializers={'w':'he_normal', 'b':'normal'})

criterion = nn.BCELoss(reduction='sum')
accuracy = dice

logFile = '../logs/'+fName+'.txt'
makeLogFile(logFile)

net.to(device)
optimizer = torch.optim.Adam(net.parameters(), lr=args.lr, weight_decay=1e-5)
nTrain = len(train_loader)
nValid = len(valid_loader)

maxAcc = 0

convIter=0
convCheck = 20

for epoch in range(args.epochs):
	trLoss = []
	vlLoss = []
	trAcc = []
	vlAcc = []
	t = time.time()

	for step, (patch, mask) in enumerate(train_loader): 
		patch = patch.to(device)
		mask = mask.to(device)
		pred = torch.sigmoid(net.forward(patch,False))
		loss = criterion(target=mask,input=pred)#/patch.shape[0]
		optimizer.zero_grad()
		loss.backward()
		optimizer.step()
		with torch.no_grad():
			acc = accuracy(pred,mask)	
			trAcc.append(acc.item())
		trLoss.append(loss.item())
		
		if (step+1) % 5 == 0:
			with torch.no_grad():
				for idx, (patch, mask) in enumerate(valid_loader): 
					patch = patch.to(device)
					mask = mask.to(device)
					pred = torch.sigmoid(net.forward(patch, False))
					loss = criterion(target=mask, input=pred)#/patch.shape[0]
					acc = accuracy(pred,mask)
					vlLoss.append(loss.item())
					vlAcc.append(acc.item())
					break
				print ('Epoch [{}/{}], Step [{}/{}], TrLoss: {:.4f}, VlLoss: {:.4f}'
					.format(epoch+1, args.epochs, step+1, 
							nTrain, trLoss[-1], vlLoss[-1]))
	epValidLoss =  np.mean(vlLoss)
	epValidAcc = np.mean(vlAcc)
	if (epoch+1) % 1 == 0 and epValidAcc > maxAcc:
		convIter = 0
		maxAcc = epValidAcc
		print("New max: %.4f\nSaving model..."%(maxAcc))
		torch.save(net.state_dict(),'../saved_models/'+fName+'.pt')
	else:
		convIter += 1
	writeLog(logFile, epoch, np.mean(trLoss), np.mean(trAcc),
					   epValidLoss,np.mean(vlAcc), time.time()-t)

	if convIter == convCheck:
		print("Converged at epoch %d"%(epoch+1-convCheck))
		break
	elif np.isnan(epValidLoss):
		print("Nan error!")
		break

