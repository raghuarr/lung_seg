# README #

This is Pytorch implementation of a U-Net with slightly different parameters than in theoriginal paper. 

### What is this repository for? ###

* Train on Shenzhen dataset to obtain lung masks
* v1.0


### How do I get set up? ###

* Basic Pytorch dependency
* Tested on Pytorch 1.3, Python 3.6 
* How to run tests: python train.py --data_path data_location

### Who do I talk to? ###

* raghav@di.ku.dk

### Thanks to the following repositories we base our project on:
* [Prob.U-Net](https://github.com/stefanknegt/Probabilistic-Unet-Pytorch) for parts of the code
