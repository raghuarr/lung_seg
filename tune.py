import torch

import numpy as np
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from data.dataset import lungData, covidData
import torch.nn as nn
from models.unet import Unet
from utils.utils import l2_regularisation,ged
import time
from utils.tools import makeLogFile, writeLog, dice_loss,dice
import pdb
import argparse
torch.manual_seed(42)
np.random.seed(42)


parser = argparse.ArgumentParser()
parser.add_argument('--epochs', type=int, default=1000, help='Number of training epochs')
parser.add_argument('--batch_size', type=int, default=2, help='Batch size')
parser.add_argument('--lr', type=float, default=1e-4, help='Learning rate')
parser.add_argument('--aug',action='store_true', default=False,help='Use data aug.')
parser.add_argument('--model', default='../saved_models/pretrained.pt',
				type=str, help='Path to model')

args = parser.parse_args()


#pdb.set_trace()
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

#data_trans, target_trans = dataTransforms(aug=args.aug) 
if args.aug:
	train_set = covidData(split='train',hflip=True,vflip=True,rot=180,p=0.1)
	valid_set = covidData(split='valid',hflip=True,vflip=True,rot=180,p=0.1)
	print("Using data augmentation....")
else:
	train_set = covidData(split='train')
	valid_set = covidData(split='valid')
	print("No augmentation...")


train_loader = DataLoader(train_set, batch_size=args.batch_size, shuffle=True)
valid_loader = DataLoader(valid_set, batch_size=args.batch_size,shuffle=True)

print("Number of train/valid patches:", 
				(len(train_set),len(valid_set)))

fName = time.strftime("%Y%m%d_%H_%M")

net = Unet(input_channels=1, num_classes=1, 
				num_filters=[16,32,64,128], 
				apply_last_layer=True, 
				padding=True, norm=True, 
				initializers={'w':'he_normal', 'b':'normal'})
net.load_state_dict(torch.load(args.model,map_location=device))
#criterion = nn.BCELoss(reduction='sum')
criterion = dice_loss

logFile = '../logs/'+fName+'.txt'
makeLogFile(logFile)

net.to(device)
optimizer = torch.optim.Adam(net.parameters(), lr=args.lr, weight_decay=1e-5)
nTrain = len(train_loader)
nValid = len(valid_loader)

maxAcc = 0

convIter=0
convCheck = 20

for epoch in range(args.epochs):
	trLoss = []
	vlLoss = []
	trAcc = []
	vlAcc = []
	t = time.time()

	for step, (patch, mask) in enumerate(train_loader): 
		patch = patch.to(device)
		mask = mask.to(device)
#		pdb.set_trace()
		pred = torch.sigmoid(net.forward(patch,False))
		loss = criterion(target=mask,input=pred)/patch.shape[0]
		optimizer.zero_grad()
		loss.backward()
		optimizer.step()
		with torch.no_grad():
			acc = dice(pred,mask)	
			trAcc.append(acc.item())
		trLoss.append(loss.item())
		
		if (step+1) % 1 == 0:
#			 pdb.set_trace()
			with torch.no_grad():
				for idx, (patch, mask) in enumerate(valid_loader): 
					patch = patch.to(device)
					mask = mask.to(device)
					pred = torch.sigmoid(net.forward(patch, False))
					loss = criterion(target=mask, input=pred)/patch.shape[0]
					acc = dice(pred,mask)
					vlLoss.append(loss.item())
					vlAcc.append(acc.item())
#					 pred = []
#					 for i in range(5):
#						 pred.append(net.sample(testing=True))
#					 pred = torch.stack(pred,1).squeeze()
#					 pred = (pred > 0.5)
#					 dGED = ged(masks,pred).mean()	 
#					 vlGed.append(dGED.item())
					break
				print ('Epoch [{}/{}], Step [{}/{}], TrLoss: {:.4f}, VlLoss: {:.4f}'
					.format(epoch+1, args.epochs, step+1, 
							nTrain, trLoss[-1], vlLoss[-1]))
	epValidLoss =  np.mean(vlLoss)
	epValidAcc = np.mean(vlAcc)
#	 pdb.set_trace()
	if (epoch+1) % 1 == 0 and epValidAcc > maxAcc:
		convIter = 0
		maxAcc = epValidAcc
		print("New max: %.4f\nSaving model..."%(maxAcc))
		torch.save(net.state_dict(),'../saved_models/'+fName+'.pt')
	else:
		convIter += 1
	writeLog(logFile, epoch, np.mean(trLoss), np.mean(trAcc),
					   epValidLoss,np.mean(vlAcc), time.time()-t)

	if convIter == convCheck:
		print("Converged at epoch %d"%(epoch+1-convCheck))
		break
	elif np.isnan(epValidLoss):
		print("Nan error!")
		break

